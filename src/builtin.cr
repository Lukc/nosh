
class Builtin
	getter input : IO
	getter output : IO
	getter fiber : Fiber
	getter name : String
	getter args : Array(AST)

	def initialize(@name, @args, environment, input, output, &block : Array(AST), IO, IO -> JSON::Any)
		input_io = if input.is_a? Process::Redirect
			# FIXME: some witchcraft’s going on here, where does
			#        “reader” disappear?
			if input.pipe?
				reader, writer = IO.pipe read_blocking: true
				writer
			else
				STDIN
			end
		else
			input
		end

		output_io_reader = nil
		output_io = if output.is_a? Process::Redirect
			if output.pipe?
				reader, writer = IO.pipe write_blocking: true
				output_io_reader = reader
				writer
			else
				STDOUT
			end
		else
			output
		end

		@input = input_io
		@output = output_io_reader || output_io

		# FIXME: This is a `select`.
		@fiber = spawn do
			block.call args, @input, @output

			input_io.close unless input_io == input || input_io == STDIN
			output_io.close unless output_io == output || output_io == STDOUT
		end
	end

	def wait
		while ! @fiber.dead?
			Fiber.yield
		end
	end
end

class Builtin::Select < Builtin
	def initialize(name, args, environment, input, output)
		super name, args, environment, input, output do |args, input, output|
			block = @args[0]?

			# FIXME: raise error? Compare literal values?
			next JSON::Any.new(nil) if block.nil?

			input.each_line do |line|
				block_environment = Environment.new environment

				block_environment[""] = begin
					JSON.parse line
				rescue
					JSON::Any.new line
				end

				block_return_value = block.execute block_environment

				if block_return_value
					output.puts line.to_json
				end
			end

			JSON::Any.new true
		end
	end
end

class Builtin::Map < Builtin
	def initialize(name, args, environment, input, output)
		super name, args, environment, input, output do |args, input, output|
			block = @args[0]?

			# FIXME: raise error? Compare literal values?
			next JSON::Any.new(nil) if block.nil?

			input.each_line do |line|
				block_environment = Environment.new environment

				block_environment[""] = begin
					JSON.parse line
				rescue
					JSON::Any.new line
				end

				block_return_value = block.execute block_environment

				unless block_return_value.is_a? Builtin || block_return_value.is_a? Process
					output.puts block_return_value.to_json
				end
			end

			JSON::Any.new true
		end
	end
end

