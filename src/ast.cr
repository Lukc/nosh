
require "./builtin.cr"

abstract class AST
	abstract def to_s

	abstract class Instruction < AST
		abstract def execute(environment, input, output)
	end

	class Pipe < Instruction
		property command1 : AST::Instruction
		property command2 : AST::Instruction

		def initialize(@command1,@command2)
		end

		def to_s
			"Pipe(#{@command1.to_s} => #{@command2.to_s})"
		end

		def execute(environment, input = Process::Redirect::Inherit, output = Process::Redirect::Inherit)
			left = command1.execute(
				environment,
				input: input,
				output: Process::Redirect::Pipe
			)

			return nil unless left

			right = command2.execute(
				environment,
				input: left.output,
				output: output
			)

			return nil unless right

			right
		end

		def wait
			command1.wait
			command2.wait
		end
	end

	class Command < Instruction
		property name : Value
		property args : Array(AST)

		def initialize(@name,@args)
		end
		
		def to_s
			"Command (#{@args.map &.to_s})"
		end

		def execute(environment, input = Process::Redirect::Inherit, output = Process::Redirect::Inherit)
			case @name.to_s
			when "select"
				@process = Builtin::Select.new name.to_s, args, environment,
					input: input,
					output: output
			when "map"
				@process = Builtin::Map.new name.to_s, args, environment,
					input: input,
					output: output
			else #FIXME Fail when block?
				@process = Process.new name.to_s, args.map &.to_s,
					input:  input,
					output: output
			end
		rescue e
			STDERR.puts e.message
		end

		def wait
			@process.try &.wait
		end
	end

	class Block < Instruction
		property instructions : Array(AST)

		def initialize(@instructions)
		end

		def to_s
			"Block(#{@instructions.map &.to_s})"
		end

		def execute(environment, input = Process::Redirect::Inherit, output = Process::Redirect::Inherit)
			@instructions.each do |thing|
				if thing.is_a? Instruction
					thing.execute environment, input, output
				else
					thing.execute environment
				end
			end
		end

	end

	abstract class Value < AST
		abstract def to_s
		def execute(environment, input = Process::Redirect::Inherit, output = Process::Redirect::Inherit)
		end

		class Literal < Value
			property val : String
			
			def initialize(@val)
			end

			def to_s
				@val
			end

			def execute(environment)
				@val
			end
		end
		
		class Variable < Value
			property name : String

			def initialize(@name)
			end

			def to_s
				@name
			end

			def execute(environment)
				environment[@name]?
			end
		end
	end

	abstract class Expr < AST
		def to_s
	  	""
		end

		# FIXME: Expressions are not processes, they don’t have IOs.
		#        That first method should be removed completely.
		def execute(environment)
		end
		
		class Nil < Expr
			def execute(env, input, output)
			end
		end

		class Unary < Expr
			property op : String
			property right : Expr | Value

			def initialize(@op, @right)
			end

			def execute(environment)
				case @op
				when "!"
					! @right.execute(environment)
				end
			end
		end

		class Operator < Expr
			property val : String
			property left : Expr | Value
			property right : Expr | Value

			def initialize(@val,@left,@right)
			end

			def execute(environment)
				lvalue = left.execute(environment)
				rvalue = right.execute(environment)

				case @val
				when "+"
					return nil unless lvalue.is_a? Number
					return nil unless rvalue.is_a? Number

					lvalue + rvalue
				end
			end
		end
	end

end 
