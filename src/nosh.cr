require "json"
#require "cbor"

require "./lexer.cr"
require "./parser.cr"
require "./environment.cr"

module Nosh
	VERSION = "0.1.0"

	tests = [
		"ls . | cat # Pipe(Command(\"ls\"), Command(\"cat\"))",
		"echo a b c",
		"echo a b c | cat",
		"echo \\\"a b c",
		"ls | sed s/cr/BLABLA/ | cat",
		"ls | map [[ $.name ]] | cat",
		"[[ a == b ]]",
		"[[ a != b ]]",
		"[[ a >= b ]]",
		"[[ a <= b ]]",
		"[[ a > b ]]",
		"[[ a < b ]]",
		"[[ 1 * 4 - 8 + 5 < 9 ]]",
		"[[ ! ( - ( 3 * -5 ) >= 3 / 5 ) ]]",

		# Point after which things stop working (correctly).
		# Because unimplemented.
		"echo a; echo b",
		"echo $PATH | cat",
		"echo ${PATH} | cat",
		"echo \"a b c\"",
		"echo \"a ${PATH} c\"",
		"ls | select [[ true ]]",
		"ls | select [[ ! true ]]",
		"ls | select [[ true ]] | cat",
		"ls | select [[ true ]] | select [[ true ]]",
		"ls | select [[ $ ~= *.cr]]", # regex comparison
		"ls | select [[ $.name == main.cr ]]",
		"ls | select [[ $.name == main.cr ]] | cat",

		# Point after which things start crashing badly.
		nil,

		"typeof unescaped-string",
		"typeof \"escaped string\"",
		"typeof 0",
		"typeof true",
		"typeof false",
		"typeof []",
		"typeof [1, 2, 3]",
		"typeof [strings, in, that, array]",
		"typeof {key: value}", # FIXME: block syntax? wtf?

		"echo \\\n\ta\\\n\tb\\\n\tc",
		"if true ; then echo true ; fi"
	]

	symbols = [
		{nil, /^[ \t]+/},
		{"eol", /^\n/},
		{nil, /^#.*/},
		{"pipe", /^\|/},
		{"eoc", /^;/},
		{"variable",/^\$([a-zA-Z0-9_]*)?/},
		# FIXME: escape sequences need to be escaped.
		{"string", /^\"((\\\\|\\"|[^\\])*)\"/},
		{"parenth start",  /^\(/},
		{"parenth end",  /^\)/},
		{"expr start", /^\[\[/},
		{"expr end", /^\]\]/},
		{"block start", /^{{/},
		{"block end", /^}}/},
		{"hash start", /^{/},
		{"hash end", /^}/},
		{"symbol", /^[^ \t\n]+/},
	]

	lexer = Lexer.new symbols
	parser = Parser.new
	environment = Environment.new

	tests.each do |input|
		break if input.nil?

		puts " > #{input}"

		(parser.parse lexer.get_tokens input).each do |instruction|
			##pp! instruction
			return_value = instruction.execute(environment).try &.wait

			if return_value.is_a? Process::Status
				return_value = return_value.exit_code
			end

			puts "[rvalue: #{return_value.to_s}]"
		end
	end
end
