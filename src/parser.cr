require "./lexer.cr"
require "./ast.cr"

class Parser
	@tokens = [] of {String, String}

	def parse(@tokens)
		index = 0
		instructions = [] of AST::Instruction

		tok, _ = get_token index

		while (tok != "eol")
			if r = is_instruction? index
				i, index = r
				instructions << i
				tok, _ = get_token index
			else
				return [] of AST::Instruction
			end
		end

		instructions
	end

	def get_token(i)
		tok = @tokens[i]?
		if (tok.nil? || tok[0] == "eol")
			{"eol",""}
		else
			tok
		end
	end

	def is_instruction?(start) : Tuple(AST::Instruction, Int32)?
		result = nil
		_end = start
		# FIXME: This made piping expressions legal. Also made a *lot* of
		#        the interpretation code very complex. Not sure whether it
		#        should be fixed here or in the execution code.
#		if r = is_expr_arithm? start
#			result, _end = r
#			tok, val = get_token(_end)
#			if tok == "eol" || tok == "eoc"
#				{result, _end + 1}
#			elsif tok == "block end"
#				{result, _end}
#			else
#				nil
#			end
		if r = is_pipe? start
			result, _end = r
			tok, val = get_token(_end)
			if tok == "eol" || tok == "eoc"
				{result, _end + 1}
			elsif tok == "block end"
				{result, _end}
			else
				nil
			end
		elsif r = is_command? start
			result, _end = r
			tok, val = get_token(_end)
			if tok == "eol" || tok == "eoc"
				{result, _end + 1}
			elsif tok == "block end"
				{result, _end}
			else
				nil
			end
		else
			nil
		end
	end

	def is_pipe?(start)
		if r = is_command?(start)
			left, _end = r
			tok, val = get_token(_end)
			if tok == "pipe"
				if r = is_instruction? _end +1
					right, _end = r
					{AST::Pipe.new(left,right), _end}
				else
					nil #FIXME incomplete command
				end
			else
				nil
			end
		else
			nil
		end
	end

	def is_command?(start)
		args = [] of AST
		index = start

		if r = is_value?(index)
			name, index = r
			while (true)
				if r = is_value? index
					arg, index = r
					args << arg
				elsif r = is_expr_arithm? index
					arg, index = r
					args << arg
				elsif r = is_block? index
					arg, index = r
					args << arg
				else
					break;
				end
			end

			{AST::Command.new(name, args) , index}
		else
			nil
		end
	end

	def is_value?(start)
		tok, val = get_token(start)
		if tok == "symbol"  || tok == "string"
			{AST::Value::Literal.new(val), start + 1}
		elsif tok == "variable"
			{AST::Value::Variable.new(val), start + 1}
		else
			nil
		end
	end

	def is_block?(start)
		tok, val = get_token(start)
		if tok == "block start"
			index = start + 1
			instructions = [] of AST

			while(true)
				if r = is_instruction? index
					i, index = r
					instructions << i
				else
					tok, _ = get_token(index)
					if tok == "block end"
						index += 1
						break
					else
						return nil #FIXME Incomplete command
					end
				end
			end

			{AST::Block.new(instructions),index}
		else
			nil
		end
	end

	def is_expr_arithm?(start)
		tok,_ = get_token(start)
		if tok == "expr start"
			index = start + 1
			if r = is_expr?(index)
				expr, index = r
				tok, _ = get_token(index)
				if tok == "expr end"
					{expr, index + 1} 
				else
					nil #FIXME Imcomplete command
				end
			else
				nil
			end
		else
			nil
		end
	end
	
	def match_op(val,level)
		ops = [
			/^\|\|/,
			/^&&/,
			/^[\=\!]=/,
			/^[\<\>]=?/,
			/^[\-\+]/,
			/^[\*\/\%]/
		]
		ops.index(level) { |op| val.matches?(op) }
	end

	def is_expr?(start, level = 0)
		left = AST::Expr::Nil.new
		index = start

		if r = is_parenth? start
			left,index = r
		elsif r = is_unary? start
			left, index = r
		elsif r = is_value? start
			left, index = r
		else
			return {left,start}
		end

		_, val = get_token(index)
		while lv = match_op(val,level)
			if r = is_expr?(index + 1,lv + 1)
				right, index = r
				left = AST::Expr::Operator.new(val,left,right)
			else
				return nil #FIXME incomplete command
			end
				
			_, val = get_token(index)
		end
		
		{left,index}
	end

	def is_unary?(start)
		_, val = get_token(start)
		if val.matches?(/^[\-\+!]/)
			if r = is_parenth?(start + 1)
				right, index = r
				{AST::Expr::Unary.new(val,right), index}
			elsif r = is_value?(start + 1)
				right, index = r
				{AST::Expr::Unary.new(val,right), index}
			else
				nil #FIXME incomplete command
			end
		else
			nil
		end
	end

	def is_parenth?(start)
		_, val = get_token(start)
		if val == "("
			if  r = is_expr?(start + 1)
				expr, index = r
				_, val = get_token(index)
				if (val == ")")
					{expr, index + 1}
				else	
					nil #FIXME incomplete command
				end
			else
				nil
			end
		else
			nil
		end
	end

	def error(msg)
		puts msg
		nil
	end
end
