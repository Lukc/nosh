
class Environment
	@parent : Environment?
	@variables = Hash(String, JSON::Any).new

	def initialize(@parent = nil)
	end

	def []?(key)
		if value = @variables[key]?
			return value
		end
		
		if (parent = @parent).nil?
			return ENV[key]?
		else
			return parent[key]?
		end
	end

	def []=(key, value)
		@variables[key] = value
	end
end

