
class Lexer
	@symbols : Array({String | Nil, Regex})

	def initialize(@symbols)
	end

	def get_tokens(input)
		tokens = [] of {String, String}

		while input.size > 0
			found_somthing = @symbols.any? do |e|
				if match = input.match e[1]
					t = e[0]
					unless t.nil?
						if m = match[1]?
							tokens << {t, m}
						else
							tokens << {t, match[0]}
						end
					end
	
					input = input[match.end, input.size]
					true
				else
					false
				end
			end

			unless found_somthing 
				tokens << {"#{input[0]}", ""}
				input = input[1, input.size]
			end
		end

		tokens
	end
end

