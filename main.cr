require "json"

require "cbor"

tests = [
	"ls . | cat # Pipe(Command(\"ls\"), Command(\"cat\"))",
	"echo a b c",
	"ls | cat | cat",

	# Point after which things stop working (correctly). Because unimplemented.
	"echo $PATH | cat",
	"echo \"a b c\"",
	"echo \"a ${PATH} c\"",
	"ls | select { bla bla }",
	"echo \\\n\ta\\\n\tb\\\n\tc",
	"if true; then echo true; fi"
]

# command "|" command => pipe
# symbol (separator symbol)* => command

# FIXME: Is this worth it? Should probably be done in a C-like language anyway.
#grammar = Grammar.new
#command = grammar.add_rule "command", [symbol]
#command = grammar.add_rule "command", [symbol, Grammar::MatchAny.new(space, symbol)]
#grammar.add_rule "pipe", [command, "|", command]

class Parser
	property tokens = [] of Tuple(String, String)

	def push_token(token)
		type, data = token

		if type == "space" || type == "comment"
			return
		end

		tokens << token
	end

	def is_pipe?(start)
		a_end = is_command? start

		return nil unless a_end

		return nil unless tokens[a_end]?.try &.[0].==("pipe")

		b_end = is_command? a_end + 1

		return nil unless b_end

		puts("pipe (#{tokens[start,a_end].map(&.[1])}) => (#{tokens[a_end+1,b_end].map(&.[1])})")

		b_end
	end

	def is_command?(start)
		index = start

		while tokens[index]?.try &.[0].==("symbol")
			index += 1
		end

		return nil if index.nil?

		puts "command #{tokens[start, index].map(&.[1])}"

		index
	end

	def parse(start = 0)
		if is_pipe?(start)
		elsif _end = is_command?(start)
			@tokens = tokens[_end, tokens.size]
		end
	end
end

tests.each do |input|
	puts "input: #{input}"

	parser = Parser.new

	while input.size > 0
		lexeme_types = [
			{"space",   /^[ \t\n]+/},
			{"pipe",    /^\|/},
			{"comment", /^#[^\n]*/},
			{"symbol",  /^[^ \t\n]+/}
		]

		found_something = lexeme_types.any? do |e|
			if match = input.match e[1]
				parser.push_token ({e[0], match[0]})

				input = input[match.end, input.size]
			end
		end

		# FIXME: should not happen. Make the code understand that.
		unless found_something
			puts "syntax error?"
			input = ""
		end
	end

	parser.parse
	puts "---"
end

exit 0 if true

STDIN.each_line do |line|
	puts JSON.parse(line).to_json
end

